/**
 * use $ node --harmony-promise-finally cryptolab-examples.web3.js
 * --harmony-promise-finally is required by nodejs because of using Promise.finally()
 */

let Rx = require('rxjs/Rx');
// let lab = require('./cryptolab.web3');
let lab = require('./cryptolab.web3');
web3 = lab.web3;

var examples = {
    showAbiConversion: () => {
        console.log(`ERC20 balanceOf(address) abi :`)

        let abiByObject = web3.eth.abi.encodeFunctionSignature({
            name: 'balanceOf',
            type: 'function',
            inputs: [{
                type: 'address',
                name: 'tokenOwner'
            }]
        })
        console.log(`Abi obtained from object signature: ${abiByObject}`)

        let abiByString = web3.eth.abi.encodeFunctionSignature('balanceOf(address)')
        console.log(`Abi obtained from string signature: ${abiByString}`)
    },
    // [BIBL]
    // https://theethereum.wiki/w/index.php/ERC20_Token_Standard
    // https://web3js.readthedocs.io/en/1.0/web3-eth-abi.html
    // https://github.com/ThatOtherZach/Web3-by-Example/wiki/Get-Token-Balance
    // https://medium.com/aigang-network/how-ethereum-contract-can-communicate-with-external-data-source-2e32616ea180 // very good explanation pull / push

    getEthereumBalance: () => {
        console.log('Getting Ethereum address info.....');

        var addr = ('0xF309D3A87CC76bB3Fe1C075968835446683242B1');

        console.log('Address:', addr);

        // Use Web3 to get the balance of the address, convert it and then show it in the console.
        console.log('Getting Ethereum address balance.....');
        web3.eth.getBalance(addr, function (error, result) {
            if (!error)
                console.log('Ether:', web3.utils.fromWei(result,'ether')); // Show the ether balance after converting it from Wei
            else
                console.log('Huston we have a promblem: ', error); // Should dump errors here
        });
    },



    getBalance: () => {
        const addr = ('0xF309D3A87CC76bB3Fe1C075968835446683242B1');
        const contractAddr = ('0xB97048628DB6B661D4C2aA833e95Dbe1A905B280');
        const tknAddress = (addr).substring(2);
        const contractData = ('0x70a08231000000000000000000000000' + tknAddress);
        const callParam = {to: contractAddr, data: contractData};


        /*
        That's how we can easily order asynchronous calls using different approaches with use of Rx.
         */

        let subject = new Rx.Subject();

        let onSuccess = result => {
            const tokens = web3.utils.toBN(result).toString();
            console.log('Tokens Owned: ' + web3.utils.fromWei(tokens, 'ether'));
        }
        // let onError = (msg) => err => console.log(err);
        let onError = msg => err => console.log(`Error -${msg}- ${err}`);
        let onFinish = (msg) => {
            return () => {
                console.log(`Finished ${msg} ..... \n`);
                subject.next({})
            }
        }
        let onErrorFinish = (msg) => {
            return err => {
                onError(msg)(err)
                onFinish(msg)()
            }
        }


        let callbackStyleCall = () => {
            const name = "Callback";
            console.log(`${name} style:`);
            web3.eth.call(callParam, function (err, result) {
                if (result) {
                    onSuccess(result)
                }
                else {
                    onError(name)(err)
                }
                onFinish(name)()
            })
        }

        let promiseStyleCall = () => {
            const name = "Promise";
            console.log(`${name} style:`);
            web3.eth.call(callParam)
                .then(onSuccess).catch(onError(name)).finally(onFinish(name))
            //for node.js use --harmony-promise-finally flag to allow Promis.finally() usage
        }

        let rxifiedFromPromiseStyleCall = () => {
            const name = "Rxified promise";
            console.log(`${name} style:`);
            // throw "Parameter is not a number!"
            // Rx.Observable.fromPromise(Promise.reject("failure"))
            Rx.Observable.fromPromise(web3.eth.call(callParam))
                .subscribe(onSuccess, onErrorFinish(name), onFinish(name));
        }

        let rxifiedFromCallbackStyleCall = () => {
            const name = "Rxified node callback ";
            console.log(`${name} style:`);
            //it's node callback because it accepts (error, value) pair as callback parameter
            Rx.Observable.bindNodeCallback(web3.eth.call)(callParam).finally(onFinish(name))
                .subscribe(onSuccess, onError(name));
            //now it's equal to the promise style call, while still using same granularity of functions passed
        }

        console.log('Getting Ethereum address balance for PAY .....\n');
        let callStyles = Rx.Observable.from([callbackStyleCall, promiseStyleCall, rxifiedFromPromiseStyleCall, rxifiedFromCallbackStyleCall])
        Rx.Observable.zip(subject, callStyles, (_, call) => call).map(call => call()).subscribe(Rx.noop, onError("Sync"))

        subject.next({})
    }
}

// examples.showAbiConversion(),
// examples.getEthereumBalance();
examples.getBalance();