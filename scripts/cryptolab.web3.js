// import Rx from 'rxjs/Rx';

var Web3 = require('web3');
var Rx = require('rxjs/Rx');


let web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/YOUR-API-TOKEN-HERE'));
exports.web3 = web3
// exports = [web3]

web3.eth.callObservable = (contractAddr, contractData) =>  Rx.Observable.fromPromise(web3.eth.call({
    to: contractAddr,
    data: contractData
}))


var addr = ('0xF309D3A87CC76bB3Fe1C075968835446683242B1');
var contractAddr = ('0xB97048628DB6B661D4C2aA833e95Dbe1A905B280');
var tknAddress = (addr).substring(2);
var contractData = ('0x70a08231000000000000000000000000' + tknAddress);


// {
//     ethCallRx: (contractAddr, contractData) => {
//
//
//
//
//
//
//         web3.eth.callObservable = (contractAddr, contractData) => {
//             return Rx.Observable.create(function (observer) {
//                 web3.eth.call({
//                     to: contractAddr,
//                     data: contractData
//                 }, function (err, result) {
//                     if (result) {
//                         var tokens = web3.utils.toBN(result).toString();
//                         console.log('Tokens Owned: ' + web3.utils.fromWei(tokens, 'ether'));
//                     }
//                     else {
//                         console.log(err); // Dump errors here
//                     }
//                 });
//
//
//                 web3.eth.getBlock(blockNumber, function (error, blockInfo) {
//                     if (error) {
//                         observer.onError(error);
//                     } else {
//                         observer.onNext(blockInfo);
//                         observer.onCompleted();
//                     }
//                 });
//             });
//         };
//
//         return Rx.Observable.create(function (observer) {
//             web3.eth.getBlock(blockNumber, function (error, blockInfo) {
//                 if (error) {
//                     observer.onError(error);
//                 } else {
//                     observer.onNext(blockInfo);
//                     observer.onCompleted();
//                 }
//             });
//         });
//
//
//     }
//     show: () => {
//
//     }
//
//
// }
