/*
A simple script that defines an address, gets the balance of it and then converts it to Ether before showing the result in the console.

For an explanation of this code, navigate to the wiki https://github.com/ThatOtherZach/Web3-by-Example/wiki/Get-Balance
*/

// Require the web3 node module.
var Web3 = require('web3');

// Show Web3 where it needs to look for a connection to Ethereum.
web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/YOUR-API-TOKEN-HERE'));

// Write to the console the script will run shortly.
console.log('Getting Ethereum address info.....');

// Define the address to search witin.
var addr = ('0xF309D3A87CC76bB3Fe1C075968835446683242B1');

// Show the address in the console.
console.log('Address:', addr);

// Use Wb3 to get the balance of the address, convert it and then show it in the console.
console.log('Getting Ethereum address balance.....');
web3.eth.getBalance(addr, function (error, result) {
	if (!error)
		console.log('Ether:', web3.utils.fromWei(result,'ether')); // Show the ether balance after converting it from Wei
	else
		console.log('Huston we have a promblem: ', error); // Should dump errors here
});

var x = () => x

var contractAddr = ('0xB97048628DB6B661D4C2aA833e95Dbe1A905B280');

var tknAddress = (addr).substring(2);

var contractData = ('0x70a08231000000000000000000000000' + tknAddress);

console.log('Getting Ethereum address balance for PAY .....');
web3.eth.call({
    to: contractAddr,
    data: contractData
}, function(err, result) {
    if (result) {
        var tokens = web3.utils.toBN(result).toString();
        console.log('Tokens Owned: ' + web3.utils.fromWei(tokens, 'ether'));
    }
    else {
        console.log(err); // Dump errors here
    }
});